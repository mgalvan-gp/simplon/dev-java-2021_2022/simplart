# Simpl'Art

Galerie d'art en ligne.

### Sommaire :

1. [Présentation du projet](#présentation-du-projet)
2. [Technologies utilisées](#technologies-utilisées)
3. [Intérets](#intérets)
4. [Installation et utilisation](#installation-et-utilisation)
5. [Pour la V2](#pour-la-v2)
6. [Aperçu en image](#aperçu)
7. [Exemples de codes utilisés](#exemples-de-codes-utilisés)
8. [License](#license)


### <span name="présentation-du-projet">Présentation du projet</span>

Projet de fin de formation Java EE.
J'ai choisi de faire une galerie d'art en ligne.

__*Temps*__
1 mois


### <span name="technologies-utilisées">Technologies utilisées</span>

* Java EE
    * Spring Boot
    * JDBC
* Thymeleaf


### <span name="intérets">Intérets</span>

Le but de ce projet était de nous familiariser avec la création d'un CRUD avec Spring Boot JDBC.<br>


### <span name="installation-et-utilisation">Installation et utilisation</span>

1. __Cloner le projet__
2. __Charger le script sql (database.sql)__
3. __Run__


### <span name="pour-la-v2">Pour la v2</span>


### <span name="aperçu">Aperçu</span>


### <span name="exemples-de-codes-utilisés">Exemples de codes utilisés</span>


### <span name="license">License</span>

Le projet et son code peuvent être utilisés sans limitations.



