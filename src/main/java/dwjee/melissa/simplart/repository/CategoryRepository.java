package dwjee.melissa.simplart.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import dwjee.melissa.simplart.entity.Category;
import dwjee.melissa.simplart.entity.Painting;

@Repository
public class CategoryRepository {
    @Autowired
    DataSource dataSource;

    /**
     * To find all categories.
     *
     * @return
     */
    public List<Category> findAll() {
        List<Category> list = new ArrayList<>();
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM category");
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                list.add(new Category(rs.getInt("id"), rs.getString("name")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return list;
    }

    /**
     * To find paintings by category.
     *
     * @param artistId
     * @return
     */
    public List<Painting> findByCategory(int categoryId) {
        List<Painting> list = new ArrayList<>();
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement("SELECT * FROM painting WHERE category_id=?");
            stmt.setInt(1, categoryId);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                list.add(
                        new Painting(
                                result.getInt("id"),
                                result.getString("title"),
                                result.getString("description"),
                                result.getString("image"),
                                result.getDate("created_at"),
                                result.getDate("updated_at")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return list;
    }

    /**
     * To find a category by id.
     *
     * @param id
     * @return
     */
    public Category findById(int id) {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement("SELECT * FROM category WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new Category(
                        result.getInt("id"),
                        result.getString("name"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return null;
    }
}
