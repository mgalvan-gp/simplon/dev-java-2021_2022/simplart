package dwjee.melissa.simplart.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

import dwjee.melissa.simplart.entity.User;

@Repository
public class UserRepository implements UserDetailsService {
    private static final String GET_USERS = "SELECT * FROM user";

    @Autowired
    private DataSource dataSource;

    /**
     * To find all users.
     * @return
     */
    public List<User> findAll() {
        Connection cnx;
        List<User> list = new ArrayList<>();
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(GET_USERS);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                list.add(new User(rs.getInt("id"), rs.getString("first_name"), rs.getString("last_name"),
                        rs.getString("email"), rs.getString("password"), rs.getDate("created_at"),
                        rs.getDate("updated_at")));
            }
            cnx.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;

    }

    /**
     * To save a user.
     * @param user
     * @return
     */
    public boolean save(User user) {
        Connection cnx;
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
                    .prepareStatement(
                            "INSERT INTO user (first_name, last_name, email, password, role) VALUES (?,?,?,?,?)",
                            PreparedStatement.RETURN_GENERATED_KEYS);

            stmt.setString(1, user.getFirstName());
            stmt.setString(2, user.getLastName());
            stmt.setString(3, user.getEmail());
            stmt.setString(4, user.getPassword());
            stmt.setString(5, user.getRole());

            if (stmt.executeUpdate() == 1) {
                ResultSet result = stmt.getGeneratedKeys();
                result.next();
                user.setId(result.getInt(1));

                return true;
            }
            cnx.close();
        } catch (SQLException e) {

            e.printStackTrace();
        }

        return false;
    }

    /**
     * To find a user by email.
     * @param email
     * @return
     */
    public User findByEmail(String email) {
        Connection cnx;
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
                    .prepareStatement("SELECT * FROM user WHERE email=?");
            stmt.setString(1, email);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new User(
                        result.getInt("id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("email"),
                        result.getString("password"),
                        result.getDate("created_at"),
                        result.getDate("updated_at"),
                        result.getString("role"));
            }
            cnx.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = findByEmail(username);
        if (user == null) {
            throw new UsernameNotFoundException("Utilisateur introuvable.");
        }
        return user;
    }

    /**
     * To delete user by id.
     * @param id
     * @return
     */
    public boolean deleteById(Integer id) {
        Connection cnx;
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement("DELETE FROM user WHERE id=?");
            stmt.setInt(1, id);
            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;

    }

    /**
     * To update a user.
     * @param user
     * @return
     */
    public boolean update(User user) {
        Connection cnx;
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(
                    "UPDATE user SET first_name=?,last_name=?, email=?, password=?, updated_at=? WHERE id=?");
            stmt.setString(1, user.getFirstName());
            stmt.setString(2, user.getLastName());
            stmt.setString(3, user.getEmail());
            stmt.setString(4, user.getPassword());
            stmt.setDate(5, user.getUpdatedAt());
            stmt.setInt(6, user.getId());
            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;

    }

}
