package dwjee.melissa.simplart.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import dwjee.melissa.simplart.entity.Painting;

@Repository
public class PaintingRepository {
    @Autowired
    DataSource dataSource;

    /**
     * To find all paintings order by desc.
     *
     * @return
     */
    public List<Painting> findAllByDesc() {
        List<Painting> list = new ArrayList<>();
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM painting ORDER BY id DESC");
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                list.add(new Painting(rs.getInt("id"), rs.getString("title"), rs.getString("description"),
                        rs.getString("image"), rs.getDate("created_at"), rs.getDate("updated_at")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return list;
    }

    /**
     * To find paintings by artist.
     *
     * @param artistId
     * @return
     */
    public List<Painting> findByArtist(int artistId) {
        List<Painting> list = new ArrayList<>();
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement("SELECT * FROM painting WHERE artist_id=?");
            stmt.setInt(1, artistId);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                list.add(
                        new Painting(
                                result.getInt("id"),
                                result.getString("title"),
                                result.getString("description"),
                                result.getString("image"),
                                result.getDate("created_at"),
                                result.getDate("updated_at")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return list;
    }

    /**
     * To find painting by id.
     *
     * @param id
     * @return
     */
    public Painting findById(int id) {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement("SELECT * FROM painting WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new Painting(
                        result.getInt("id"),
                        result.getString("title"),
                        result.getString("description"),
                        result.getString("image"),
                        result.getDate("created_at"),
                        result.getDate("updated_at"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return null;
    }

    /**
     * To save a painting.
     *
     * @param painting
     * @return
     */
    public boolean save(Painting painting) {
        Connection cnx;
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
                    .prepareStatement("INSERT INTO painting (title, description, image, artist_id) VALUES (?, ?, ?, ?)",
                            PreparedStatement.RETURN_GENERATED_KEYS);

            stmt.setString(1, painting.getTitle());
            stmt.setString(2, painting.getDescription());
            stmt.setString(3, painting.getImage());
            stmt.setInt(4, painting.getArtist().getId());

            if (stmt.executeUpdate() == 1) {
                ResultSet result = stmt.getGeneratedKeys();
                result.next();
                painting.setId(result.getInt(1));

                return true;
            }
            cnx.close();
        } catch (SQLException e) {

            e.printStackTrace();
        }

        return false;
    }
}
