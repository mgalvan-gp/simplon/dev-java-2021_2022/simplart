package dwjee.melissa.simplart.Service;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
public class Uploader {
    public String upload(MultipartFile file) throws Exception {
        String filePath = "/uploads/" + UUID.randomUUID() + "."
                + StringUtils.getFilenameExtension(file.getOriginalFilename());

        Path path = Paths.get("src/main/resources/static" + filePath);
        Files.copy(file.getInputStream(), path);

        return filePath;

    }
}
