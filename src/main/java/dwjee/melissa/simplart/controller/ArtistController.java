package dwjee.melissa.simplart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import dwjee.melissa.simplart.Service.Uploader;
import dwjee.melissa.simplart.entity.Artist;
import dwjee.melissa.simplart.entity.Painting;
import dwjee.melissa.simplart.repository.ArtistRepository;
import dwjee.melissa.simplart.repository.PaintingRepository;

@Controller
@RequestMapping("/artist")
public class ArtistController {
    @Autowired
    private ArtistRepository artistRepo;
    @Autowired
    private PaintingRepository paintingRepo;
    @Autowired
    private Uploader uploader;

    /**
     * Return artist account with his paintings.
     *
     * @param model
     * @param authentication
     * @return
     */
    @GetMapping("/account")
    public String getAllPaintings(Model model, Authentication authentication) {
        Artist artist = (Artist) authentication.getPrincipal();
        model.addAttribute("artist", artistRepo.findById(artist.getId()));
        model.addAttribute("paintings", paintingRepo.findByArtist(artist.getId()));
        return "artist-account";
    }

    /**
     * Return the user-register page.
     *
     * @param model
     * @return
     */
    @GetMapping("/add-painting")
    public String showAddPainting(Model model) {
        model.addAttribute("painting", new Painting());
        return "painting/add-painting";
    }

    /**
     * To save a painting.
     *
     * @param painting
     * @return
     */
    @PostMapping("/add-painting")
    public String addPainting(Painting painting, Authentication authentication,
            @RequestParam("file") MultipartFile file) {

        try {
            painting.setImage(uploader.upload(file));

            Artist artist = (Artist) authentication.getPrincipal();
            painting.setArtist(artist);
            paintingRepo.save(painting);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:/artist/account";
    }
}
