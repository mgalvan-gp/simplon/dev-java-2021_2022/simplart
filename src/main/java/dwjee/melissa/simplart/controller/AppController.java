package dwjee.melissa.simplart.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import dwjee.melissa.simplart.entity.Artist;
import dwjee.melissa.simplart.entity.User;
import dwjee.melissa.simplart.repository.ArtistRepository;
import dwjee.melissa.simplart.repository.CategoryRepository;
import dwjee.melissa.simplart.repository.PaintingRepository;
import dwjee.melissa.simplart.repository.UserRepository;

@Controller
public class AppController  implements ErrorController {
    @Autowired
    private ArtistRepository artistRepo;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private PaintingRepository paintingRepo;

    @Autowired
    private CategoryRepository categoryRepo;

    /**
     * Return the index page with all paintings.
     *
     * @param model
     * @return
     */
    @GetMapping("/")
    public String showHome(Model model) {
        model.addAttribute("paintings", paintingRepo.findAllByDesc());
        model.addAttribute("categories", categoryRepo.findAll());
        return "index";
    }

    /**
     * Return the artist-register page with a form.
     *
     * @param model
     * @return
     */
    @GetMapping("artist-register")
    public String showArtistRegister(Model model) {
        model.addAttribute("artist", new Artist());
        return "artist-register";
    }

    /**
     * To save an artist.
     *
     * @param artist
     * @param model
     * @return
     */
    @PostMapping("artist-register")
    public String registerArtist(@Valid Artist artist, BindingResult binding, Model model) {

        if (artistRepo.findByEmail(artist.getEmail()) != null) {
            model.addAttribute("feedback", "Cet artiste existe déjà.");
            return "artist-register";
        }
        if (binding.hasErrors()) {
            return "artist-register";
        }
        String hashedPassword = encoder.encode(artist.getPassword());
        artist.setPassword(hashedPassword);
        artist.setRole("ROLE_ARTIST");
        artistRepo.save(artist);
        return "redirect:/login";
    }

    /**
     * Return the user-register page.
     *
     * @param model
     * @return
     */
    @GetMapping("/user-register")
    public String showUserRegister(Model model) {
        model.addAttribute("user", new User());
        return "user-register";
    }

    /**
     * To save a user.
     *
     * @param user
     * @param model
     * @return
     */
    @PostMapping("/user-register")
    public String registerUser(@Valid User user, BindingResult binding, Model model) {
        if (userRepo.findByEmail(user.getEmail()) != null) {
            model.addAttribute("feedback", "Cet utilisateur existe déjà.");
            return "user-register";
        }
        if (binding.hasErrors()) {
            return "user-register";
        }
        String hashedPassword = encoder.encode(user.getPassword());
        user.setPassword(hashedPassword);
        user.setRole("ROLE_USER");
        userRepo.save(user);
        return "redirect:/login";
    }

    /**
     * Return login page.
     *
     * @return
     */
    @GetMapping("/login")
    public String login() {
        return "login";
    }

    /**
     * To redirect to a specific page by role after login.
     *
     * @param request
     * @return
     */
    @GetMapping("/default")
    public String defaultAfterLogin(HttpServletRequest request) {
        if (request.isUserInRole("ROLE_ARTIST")) {
            return "redirect:/artist/account";
        }
        if (request.isUserInRole("ROLE_ADMIN")) {
            return "redirect:/admin/account";
        }
        return "redirect:/user/account";
    }

    /**
     * To manage error pages.
     *
     * @param request
     * @return
     */
    @GetMapping("/error")
    public String handleError(HttpServletRequest request) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

        if (status != null) {
            Integer statusCode = Integer.valueOf(status.toString());

            if (statusCode == HttpStatus.NOT_FOUND.value()) {
                return "404";
            } else if (statusCode == HttpStatus.FORBIDDEN.value()) {
                return "403";
            }
        }
        return "error";
    }
}
