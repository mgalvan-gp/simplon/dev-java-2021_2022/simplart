package dwjee.melissa.simplart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import dwjee.melissa.simplart.repository.PaintingRepository;

@Controller
public class PaintingController {
    @Autowired
    private PaintingRepository paintingRepo;

    /**
     * Return painting by id.
     *
     * @param id
     * @param model
     * @return
     */
    @GetMapping("/paintings/{id}")
    public String showPaintingById(@PathVariable("id") int id, Model model) {
        model.addAttribute("painting", paintingRepo.findById(id));
        return "painting/one-painting";
    }
}
