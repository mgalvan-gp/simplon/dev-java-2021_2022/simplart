package dwjee.melissa.simplart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimplartApplication {
	public static void main(String[] args) {
		SpringApplication.run(SimplartApplication.class, args);
	}

}
