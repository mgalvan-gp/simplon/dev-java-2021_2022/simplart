INSERT INTO
  user (first_name, last_name, email, password, role)
VALUES
  (
    'Nicolas',
    'Large',
    'nico@mail.fr',
    '$2a$10$dVENM9brYsZ4QQn9ALwTwevvxhRwuLBRZLBZ3k/P3jrhz5lcBbY6i',
    'ROLE_USER'
  );