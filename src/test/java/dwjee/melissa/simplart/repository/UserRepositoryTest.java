package dwjee.melissa.simplart.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import dwjee.melissa.simplart.entity.User;

@ActiveProfiles("test")
@SpringBootTest
@Sql(scripts={"/schema.sql", "/data.sql"}, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
public class UserRepositoryTest {

    @Autowired
    UserRepository userRepo;

    @Test
    void testDeleteUser() {
        assertTrue(userRepo.deleteById(1));
    }

    @Test
    void testSaveUser() {
        User user = new User("Melissa", "Galvan", "mel@mail.fr", "$2a$10$dVENM9brYsZ4QQn9ALwTwevvxhRwuLBRZLBZ3k/P3jrhz5lcBbY6i", "ROLE_USER");
        assertTrue(userRepo.save(user));
        assertEquals(2, user.getId());
        assertEquals("ROLE_USER", user.getRole());
    }

    @Test
    void testUpdateUser() {
        User user = new User(1, "Nico", "Large", "nico@mail.com", "$2a$10$dVENM9brYsZ4QQn9ALwTwevvxhRwuLBRZLBZ3k/P3jrhz5lcBbY6i","ROLE_ADMIN");
        assertTrue(userRepo.update(user));
    }

    @Test
    void testFindByEmail() {
        User user = userRepo.findByEmail("nico@mail.fr");
        assertEquals("nico@mail.fr", user.getEmail());
        assertEquals("ROLE_USER", user.getRole());
    }

    @Test
    void testFindAll() {
        assertNotEquals(0, userRepo.findAll().size());
    }

}
